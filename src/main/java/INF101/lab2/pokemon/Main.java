package INF101.lab2.pokemon;

import java.util.Random;

public class Main {

    public static Pokemon pokemon1;
    public static Pokemon pokemon2;
    public static void main(String[] args) {
        ///// Oppgave 6
        // Have two pokemon fight until one is defeated

        // Initializing pokemons
        pokemon1 = new Pokemon("Pikachu", 94, 12);
        pokemon2 = new Pokemon("Oddish", 100, 3);

        // Letting them figth until one is defeated
        while (pokemon1.isAlive() && pokemon2.isAlive()) {
            // Draws a random number 1 or 2 to see who gets to attack
            Random rnd = new Random();
            int attackerNumber = rnd.nextInt(1,3);

            // Attacking
            if (attackerNumber == 1) {
                pokemon1.attack(pokemon2);
            }
            else {
                pokemon2.attack(pokemon1);
            }
        }
    }
}
