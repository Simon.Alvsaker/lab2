package INF101.lab2.pokemon;

import java.util.Random;

public class Pokemon {
    ////// Oppgave 1a
    // Create field variables here:
    private String name;
    private int healthPoints;
    private int maxHealthPoints;
    private int strength;

    ///// Oppgave 1b
    // Create a constructor here:
    public Pokemon(String name, int maxHealthPoints, int strength) {
        this.name = name;
        this.healthPoints = maxHealthPoints;
        this.maxHealthPoints = maxHealthPoints;
        this.strength = strength;
    }

    ///// Oppgave 2
	/**
     * Get name of the pokémon
     * @return name of pokémon
     */
    String getName() {
        return name;
    }

    /**
     * Get strength of the pokémon
     * @return strength of pokémon
     */
    int getStrength() {
        return strength;
    }

    /**
     * Get current health points of pokémon
     * @return current HP of pokémon
     */
    int getCurrentHP() {
        return healthPoints;
    }
    
    /**
     * Get maximum health points of pokémon
     * @return max HP of pokémon
     */
    int getMaxHP() {
        return maxHealthPoints;
    }

    /**
     * Check if the pokémon is alive. 
     * A pokemon is alive if current HP is higher than 0
     * @return true if current HP > 0, false if not
     */
    boolean isAlive() {
        if (healthPoints > 0) {
            return true;
        }
        else {
            return false;
        }
    }


    
    ///// Oppgave 4
    /**
     * Damage the pokémon. This method reduces the number of
     * health points the pokémon has by <code>damageTaken</code>.
     * If <code>damageTaken</code> is higher than the number of current
     * health points then set current HP to 0.
     *
     * It should not be possible to deal negative damage, i.e. increase the number of health points.
     *
     * The method should print how much HP the pokemon is left with.
     *
     * @param damageTaken
     */
    void damage(int damageTaken) {
        // Checks if damageTaken is positive -> continue, ngative -> break
        if (damageTaken < 0) {
            return;
        }

        // Sets HP to 0 if damageTaken >= HP
        if (damageTaken >= healthPoints) {
            healthPoints = 0;
        }
        // Else: Reduces healthpoints
        else {
            healthPoints -= damageTaken;
        }
        // Prints out remaining HP
        String remainingHPString = String.format("%s takes %d damage and is left with %d/%d HP", name, damageTaken, healthPoints, maxHealthPoints);
        System.out.println(remainingHPString);
    }

    ///// Oppgave 5
    /**
     * Attack another pokémon. The method conducts an attack by <code>this</code>
     * on <code>target</code>. Calculate the damage using the pokémons strength
     * and a random element. Reduce <code>target</code>s health.
     * 
     * If <code>target</code> has 0 HP then print that it was defeated.
     * 
     * @param target pokémon that is being attacked
     */
    void attack(Pokemon target) {
        // Creates random damage - a random int from 0 to strength of attacker
        Random rnd = new Random();
        int damage = rnd.nextInt(strength + 1);

        // Calls damage() to reduce the HP of target
        target.damage(damage);

        // Checking if HP is 0
        if (!target.isAlive()) {
            System.out.println(target.name + " is defated by " + name);
        }
    }

    ///// Oppgave 3
    @Override
    public String toString() {
        String pokemonString = String.format("%s HP: (%d/%d) STR: %d", name, healthPoints, maxHealthPoints, strength);
        return pokemonString;
    }

}
